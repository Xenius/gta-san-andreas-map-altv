import alt from 'alt';
import game from 'natives';

alt.everyTick(() => {
	game.extendWorldBoundaryForPlayer(-100000, -100000, 100);
	game.extendWorldBoundaryForPlayer(100000, 100000, 100);
});